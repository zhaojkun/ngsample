import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable()
export class AppComponent {
  title = 'app';
  constructor(private http:HttpClient){}

  ngInit(){

  }
  getDevices(){
    this.http.get("/api/orders").subscribe()
  }
}
